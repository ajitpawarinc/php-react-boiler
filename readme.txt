---------------------------------------------------------
Prerequisites
You must have the following prerequsites in order to run this project

Knowledge of PHP and MySQL,
Knowledge of JavaScript and React,
PHP and MySQL installed on your development machine.
---------------------------------------------------------
Database Creation 

create database reactdb;

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city` varchar(100),
  `country` varchar(100),
  `job` varchar(100)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

---------------------------------------------------------
Local PHP Server
run this command under Project Root to run application on localhost
php -S 127.0.0.1:8080
---------------------------------------------------------
Frontend is served from
http://127.0.0.1:8080/

Backend API call to get data or save data using axios library
http://127.0.0.1:8080/api/contacts.php
---------------------------------------------------------
Referance
https://www.techiediaries.com/php-react-rest-api-crud-tutorial/
---------------------------------------------------------
